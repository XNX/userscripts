// ==UserScript==
// @name        1337x Sort & Filter
// @namespace   https://codeberg.org/XNX/userscripts/1337x
// @match       https://1337x.to/*
// @grant       none
// @version     1.0
// @author      XeNoX
// @description Fix 1337x sort and filter options when JavaScript is disabled
// @run-at      document-end
// ==/UserScript==

var x = document.querySelectorAll("select");

if (x[0]) {
  x[0].addEventListener('change', function() { top.location.href = this.options[this.selectedIndex].value; });
  if (x[1]) x[1].addEventListener('change', function() { top.location.href = this.options[this.selectedIndex].value; });
}
