// ==UserScript==
// @name        1337x display fix
// @namespace   https://codeberg.org/XNX/userscripts/1337x
// @match       https://1337x.to/*
// @grant       none
// @version     1.0
// @author      XeNoX
// @description Fix 1337x when JavaScript is disabled
// @run-at      document-end
// ==/UserScript==

if(self===top){var a=document.getElementById('antiClickjack');a.parentNode.removeChild(a);}else{top.location=self.location;}
