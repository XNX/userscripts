// ==UserScript==
// @name        ChatGPT re-enable devtools shortcut
// @namespace   https://codeberg.org/XNX/userscripts/ChatGPT
// @match       https://chatgpt.com/*
// @grant       none
// @version     1.0
// @author      XeNoX
// @description Prevent the ChatGPT website from overriding Ctrl+Shift+I
// @run-at      document-idle
// ==/UserScript==

document.addEventListener('keydown', function(evt) {
  if (evt.ctrlKey && evt.shiftKey && evt.key == 'I') {
    evt.stopImmediatePropagation();
  }
}, true);
