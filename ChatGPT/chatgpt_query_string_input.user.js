// ==UserScript==
// @name        ChatGPT Query String Input
// @namespace   https://codeberg.org/XNX/userscripts/ChatGPT
// @match       https://chatgpt.com/*
// @grant       none
// @version     2.1
// @author      XeNoX
// @description Prefill prompt with query from text parameter, and optionally send. It is recommended to change the key to a new, random one, otherwise websites could make you send unwanted messages. This can be added as a custom search engine (e.g. https://chatgpt.com/?temporary-chat=true&text=%s&[KEY1]=[KEY2]), so that (for simple queries) you can use ChatGPT right from the address bar.
// @run-at      document-start
// ==/UserScript==

var params = new URLSearchParams(window.location.search);
var input = params.get('text');

if (input) {
  var observer = new MutationObserver(function(m) {
    m.forEach(function(item) {
      var node = item?.addedNodes[0]?.nextSibling?.childNodes[0];
      if (node?.id == 'prompt-textarea') {
        setTimeout(function() {
          node.value = input;
          node.dispatchEvent(new Event('input', { bubbles: true, cancelable: false }));
          if (params.get("kBQPaskM7Hat8oWWCvRUtrbRRDy6p28hRuIn1zPx83JDHiH9Ic1sx5FujIcRYR63") === "x3Q00Z6vj4Y--OnorWTqPUXixTUVZ-7mCjF_f2laKDd5-T8sSIbrYbKwbSOL2Wo7") {
            setTimeout(function() { node.parentElement.nextSibling.click(); });
          }
        }, 10);
      }
    });
  });
  observer.observe(document.documentElement, { childList: true, subtree: true });
}
