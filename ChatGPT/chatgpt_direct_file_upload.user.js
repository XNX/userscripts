// ==UserScript==
// @name        ChatGPT Direct File Upload
// @namespace   https://codeberg.org/XNX/userscripts/ChatGPT
// @match       https://chatgpt.com/*
// @grant       none
// @version     1.0
// @author      XeNoX
// @description Bypass submenu and directly upload file
// @run-at      document-idle
// ==/UserScript==

var observer = new MutationObserver(function(m) {
  m.forEach(function(item) {
    var x = item.addedNodes[0] && item.addedNodes[0].childNodes[0] && item.addedNodes[0].childNodes[0].childNodes;
    if (x && x.length == 4 && x[3].className == 'flex items-center m-1.5 p-2.5 text-sm cursor-pointer focus-visible:outline-0 radix-disabled:pointer-events-none radix-disabled:opacity-50 group relative hover:bg-[#f5f5f5] focus-visible:bg-[#f5f5f5] dark:hover:bg-token-main-surface-secondary dark:focus-visible:bg-token-main-surface-secondary rounded-md my-0 px-3 mx-2 radix-state-open:bg-[#f5f5f5] dark:radix-state-open:bg-token-main-surface-secondary gap-2.5 py-3') x[3].click();
  });
});
observer.observe(document.body, { childList: true });
