// ==UserScript==
// @name        ChatGPT No send on Enter
// @namespace   https://codeberg.org/XNX/userscripts/ChatGPT
// @match       https://chatgpt.com/*
// @grant       none
// @version     1.0
// @author      XeNoX
// @description Prevents Enter key from sending message. You must use Ctrl+Enter instead.
// @run-at      document-idle
// ==/UserScript==

document.addEventListener('keydown', function(evt) {
  evt.target.id == 'prompt-textarea' && !evt.ctrlKey && evt.key == 'Enter' && evt.stopPropagation();
}, true);
