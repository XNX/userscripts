// ==UserScript==
// @name        ChatGPT Fullwidth
// @namespace   https://codeberg.org/XNX/userscripts/ChatGPT
// @match       https://chatgpt.com/*
// @grant       GM.addStyle
// @version     1.0
// @author      XeNoX
// @description Make messages expand to the entire screen
// @run-at      document-end
// ==/UserScript==

GM.addStyle(`.text-token-text-primary>div>div{max-width:100%!important}`);
