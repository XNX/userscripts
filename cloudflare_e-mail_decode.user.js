// ==UserScript==
// @name        Cloudflare e-mail decode
// @namespace   https://codeberg.org/XNX/userscripts
// @match       https://*/*
// @grant       none
// @version     1.0
// @author      XeNoX
// @description Decode e-mail addresses obfuscated by Cloudflare even when JavaScript is disabled. Also fuck Cloudflare.
// @run-at      document-end
// ==/UserScript==

!function(){"use strict";var x=document,d=x.createElement("div");function r(e,t){var r=e.slice(t,t+2);return parseInt(r,16);}function n(n,c){for(var o="",a=r(n,c),i=c+2;i<n.length;i+=2){var l=r(n,i)^a;o+=String.fromCharCode(l);}/*o = decodeURIComponent(escape(o));*/d.innerHTML=`<a href="${o.replaceAll('"',"&quot;")}"></a>`;return d.childNodes[0].getAttribute("href")||"";}function c(t){for(var r=t.querySelectorAll("a"),c=0;c<r.length;c++){var o=r[c],a=o.href.indexOf("/cdn-cgi/l/email-protection#");a>-1&&(o.href="mailto:"+n(o.href,a+28));}}function o(t){for(var r=t.querySelectorAll(".__cf_email__"),c=0;c<r.length;c++){var o=r[c],a=o.parentNode,i=o.getAttribute("data-cfemail");if(i){var l=n(i,0),d=x.createTextNode(l);a.replaceChild(d,o);}}}function a(t){for(var r=t.querySelectorAll("template"),n=0;n<r.length;n++)i(r[n].content);}function i(t){c(t),o(t),a(t);}i(x);}();
